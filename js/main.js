$('.multiple-items').slick({
    // centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    variableWidth: true
  });

  $('.data-science').slick({
    slidesToShow: 3
  });
  


  $('.homeslider').slick({
    dots: true,
    arrows: false
  });

  

  $('.gallery').slick({
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    variableWidth: true
  });

  $('.m-success-stories-slider').slick({
    dots: true,
    arrows: false
  });



$(function() {
  $(".dropdown").hover(
      function(){ $(this).addClass('show') },
      function(){ $(this).removeClass('show') },
  );
  
});


// $(document).on('mouseenter', '[data-toggle="tab"]', function () {
//   $(this).tab('show');
// });


$('.courses-tabs > .nav-tabs > li ').hover(function() {
  if ($(this).hasClass('hoverblock'))
    return;
  else
    $(this).find('a').tab('show');
});

$('.courses-tabs > .nav-tabs > li').find('a').click(function() {
  $(this).parent()
    .siblings().addClass('hoverblock');
});




$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
  e.target
  e.relatedTarget
  $('.data-science').slick('setPosition');
});


$(document).ready(function() {     
  $('.dropdown').hover(function(){     
      $('.overlay').addClass('show');    
  },     
  function(){    
      $('.overlay').removeClass('show');     
  });
});   