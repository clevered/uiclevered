$('.multiple-items').slick({
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    variableWidth: true
  });

  $('.data-science').slick({
    slidesToShow: 3
  });
  

  $('.gallery').slick({
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    variableWidth: true
  });

  $('.success-stories-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    variableWidth: true
  });

  $('.mobile-success-stories').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false
  });


  $('.course__slider').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    infinite: false,
    variableWidth: true,

    responsive: [
     
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows: true
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  $(function() {
    $(".dropdown").hover(
        function(){ $(this).addClass('show') },
        function(){ $(this).removeClass('show') }
    );
});




$('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
  e.target
  e.relatedTarget
  $('.data-science').slick('setPosition');
});